/**
 * \file
 */

#include <stdint.h>
#include "config_counter.h"	/* for COUNTER_MAX macro constant */
#include "pins.h"	/* for CNT_IN and CNT_IN_* macro constants */

/*
 * Private Macro Constants
 */

/**
 * \brief Configure pin to be used as counter input as input.
 */
#define CNT_IN_AS_INPUT		(CNT_IN_DDR &= ~(1 << CNT_IN))
/**
 * \brief Enable Pull-Up resistor for pin used as counter input.
 */
#define CNT_IN_PULLUP_ON	(CNT_IN_PORT |= (1 << CNT_IN))

/**
 * \brief Read current state of counter input pin.
 */
#define CNT_IN_READ		(CNT_IN_PIN & (1 << CNT_IN))

/*
 * Private Variables
 */

/**
 * \brief Contains the current counter value.
 */
static uint8_t counter;

/*
 * Private functions
 */

static void cnt_inpin_init(void)
{
	CNT_IN_AS_INPUT;
	CNT_IN_PULLUP_ON;
}

static void cnt_reset(void)
{
	counter = 0;
}

static void cnt_advance(void)
{
	counter++;
}

/*
 * Public Functions
 */

void counter_init(void)
{
	/* Reset counter value. */
	cnt_reset();

	/* Setup counter input pin. */
	cnt_inpin_init();
}

void counter_reset(void)
{
	cnt_reset();
}

void counter_tick(void)
{
	if(CNT_IN_READ == 0)
	{
		if(counter < COUNTER_MAX)	cnt_advance();
		else				cnt_reset();
	}
}

uint8_t counter_read(void)
{
	return counter;
}
