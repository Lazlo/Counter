/**
 * \file
 */

#ifndef D_DISP_MACROS_H
#define D_DISP_MACROS_H

/*
 * Macros
 */

#if SEG_LED_MODE!=SEG_LED_COM_VCC && SEG_LED_MODE!=SEG_LED_COM_GND
#error "SEG_LED_MODE must either be SEG_LED_COM_VCC or SEG_LED_COM_GND!"
#endif

/* Configure pin direction */

#define DIGIT_2_AS_OUTPUT	(DIGIT_2_DDR |= (1 << DIGIT_2))		/**< Configure pin for digit 2 as output. */
#define DIGIT_1_AS_OUTPUT	(DIGIT_1_DDR |= (1 << DIGIT_1))		/**< Configure pin for digit 1 as output. */

#if SEG_LED_MODE==SEG_LED_COM_VCC

	/*
	 * For Common Anode (VCC)
	 */

	#define LED_DP_AS_INPUT	(LED_DP_DDR |= (1 << LED_DP))
	#define LED_G_AS_INPUT		(LED_G_DDR &= ~(1 << LED_G))
	#define LED_F_AS_INPUT		(LED_F_DDR &= ~(1 << LED_F))
	#define LED_E_AS_INPUT		(LED_E_DDR &= ~(1 << LED_E))
	#define LED_D_AS_INPUT		(LED_D_DDR &= ~(1 << LED_D))
	#define LED_C_AS_INPUT		(LED_C_DDR &= ~(1 << LED_C))
	#define LED_B_AS_INPUT		(LED_B_DDR &= ~(1 << LED_B))
	#define LED_A_AS_INPUT		(LED_A_DDR &= ~(1 << LED_A))

#elif SEG_LED_MODE==SEG_LED_COM_GND

	/*
	 * For Common Cathode (GND)
	 */

	#define LED_DP_AS_OUTPUT	(LED_DP_DDR |= (1 << LED_DP))
	#define LED_G_AS_OUTPUT		(LED_G_DDR |= (1 << LED_G))
	#define LED_F_AS_OUTPUT		(LED_F_DDR |= (1 << LED_F))
	#define LED_E_AS_OUTPUT		(LED_E_DDR |= (1 << LED_E))
	#define LED_D_AS_OUTPUT		(LED_D_DDR |= (1 << LED_D))
	#define LED_C_AS_OUTPUT		(LED_C_DDR |= (1 << LED_C))
	#define LED_B_AS_OUTPUT		(LED_B_DDR |= (1 << LED_B))
	#define LED_A_AS_OUTPUT		(LED_A_DDR |= (1 << LED_A))

#endif

/* Set pin level */

#define LED_DP_OFF		(LED_DP_PORT &= ~(1 << LED_DP))		/**< Turn LED for DP segment off. */
#define LED_DP_ON		(LED_DP_PORT |= (1 << LED_DP))		/**< Turn LED for DP segment on. */

#define LED_G_OFF		(LED_G_PORT &= ~(1 << LED_G))		/**< Turn LED for G segment off. */
#define LED_G_ON		(LED_G_PORT |= (1 << LED_G))		/**< Turn LED for G segment on. */

#define LED_F_OFF		(LED_F_PORT &= ~(1 << LED_F))		/**< Turn LED for F segment off. */
#define LED_F_ON		(LED_F_PORT |= (1 << LED_F))		/**< Turn LED for F segment on. */

#define LED_E_OFF		(LED_E_PORT &= ~(1 << LED_E))		/**< Turn LED for E segment off. */
#define LED_E_ON		(LED_E_PORT |= (1 << LED_E))		/**< Turn LED for E segment on. */


#define LED_D_OFF		(LED_D_PORT &= ~(1 << LED_D))		/**< Turn LED for D segment off. */
#define LED_D_ON		(LED_D_PORT |= (1 << LED_D))		/**< Turn LED for D segment on. */

#define LED_C_OFF		(LED_C_PORT &= ~(1 << LED_C))		/**< Turn LED for C segment off. */
#define LED_C_ON		(LED_C_PORT |= (1 << LED_C))		/**< Turn LED for C segment on. */

#define LED_B_OFF		(LED_B_PORT &= ~(1 << LED_B))		/**< Turn LED for B segment off. */
#define LED_B_ON		(LED_B_PORT |= (1 << LED_B))		/**< Turn LED for B segment on. */

#define LED_A_OFF		(LED_A_PORT &= ~(1 << LED_A))		/**< Turn LED for A segment off. */
#define LED_A_ON		(LED_A_PORT |= (1 << LED_A))		/**< Turn LED for A segment on. */



#define DIGIT_2_OFF		(DIGIT_2_PORT |= (1 << DIGIT_2))	/**< Turn digit 2 off. */
#define DIGIT_2_ON		(DIGIT_2_PORT &= ~(1 << DIGIT_2))	/**< Turn digit 2 on. */

#define DIGIT_1_OFF		(DIGIT_1_PORT |= (1 << DIGIT_1))	/**< Turn digit 1 off. */
#define DIGIT_1_ON		(DIGIT_1_PORT &= ~(1 << DIGIT_1))	/**< Turn digit 1 on. */

#endif /* D_DISP_MACROS_H */
