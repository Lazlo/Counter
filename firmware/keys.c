/**
 * \file
 * \brief Keys module implementation
 */

/*
 * Includes
 */
#include "pins.h"	/* for SW2, SW2_*, SW1 and SW1_* macro constants */

/*
 * Private Macro Constants
 */

#define SW2_AS_INPUT	(SW2_DDR &= ~(1 << SW2))	/**< Configure switch 2 pin as input. */

#define SW1_AS_INPUT	(SW1_DDR &= ~(1 << SW1))	/**< Configure switch 1 pin as input. */

#define SW2_PULLUP_ON	(SW2_PORT |= (1 << SW2))	/**< Enable pull-up resistor for switch 2 input pin. */

#define SW1_PULLUP_ON	(SW1_PORT |= (1 << SW1))	/**< Enable pull-up resistor for switch 1 input pin. */

#define LOW	0	/**< Symbolic name for pin input level LOW (GND). */

#define HIGH	1	/**< Symbolic name for pin input level HIGH (VCC). */

/**
 * \brief Read switch 2.
 */
#define SW2_READ	(SW2_PIN & (1 << SW2))

/**
 * \brief Read switch 1.
 */
#define SW1_READ	(SW1_PIN & (1 << SW1))

/**
 * \brief Check if switch 2 was pressed.
 */
#define SW2_PRESSED	(SW2_READ == LOW)

/**
 * \brief Check if switch 1 was pressed.
 */
#define SW1_PRESSED	(SW1_READ == LOW)
/*
 * Public Functions
 */

void keys_init(void)
{
	/* Configure pins to be used as input for keys. */
	SW2_AS_INPUT;
	SW1_AS_INPUT;

	SW2_PULLUP_ON;
	SW1_PULLUP_ON;
}

uint8_t keys_read(void)
{
	/* Space for return value */
	uint8_t rv;

	/* Initialize with no bits set (default for active-high or positive logic) */
	rv = 0;

	/* Read switches and set the respective bit (positive logic) when switch is active. */
	if(SW2_PRESSED)	rv |= (1 << 1);
	if(SW1_PRESSED)	rv |= (1 << 0);

	return rv;
}
