/**
 * \file
 */

/*
 * Includes
 */

#include "pins.h"
#include "disp.h"
#include "disp_macros.h"

/*
 * Public Functions
 */

void seg_led_init(void)
{
	/* Setup direction for 7-segment select pins. */
	DIGIT_2_AS_OUTPUT;
	DIGIT_1_AS_OUTPUT;

	/* Setup direction for 7-segment LED pins. */
#if SEG_LED_MODE==SEG_LED_COM_VCC
	LED_DP_AS_INPUT;
	LED_G_AS_INPUT;
	LED_F_AS_INPUT;
	LED_E_AS_INPUT;

	LED_D_AS_INPUT;
	LED_C_AS_INPUT;
	LED_B_AS_INPUT;
	LED_A_AS_INPUT;
#elif SEG_LED_MODE==SEG_SED_COM_GND
	LED_DP_AS_OUTPUT;
	LED_G_AS_OUTPUT;
	LED_F_AS_OUTPUT;
	LED_E_AS_OUTPUT;

	LED_D_AS_OUTPUT;
	LED_C_AS_OUTPUT;
	LED_B_AS_OUTPUT;
	LED_A_AS_OUTPUT;
#endif
}

void seg_led_off(const uint8_t digit)
{
	if(digit == 0)
	{
		DIGIT_2_OFF;
		DIGIT_1_OFF;
	}
	else if(digit == 1)
	{
		DIGIT_1_OFF;
	}
	else if(digit == 2)
	{
		DIGIT_2_OFF;
	}
}

void seg_led_on(const uint8_t digit)
{
	if(digit == 0)
	{
		DIGIT_2_ON;
		DIGIT_1_ON;
	}
	else if(digit == 1)
	{
		DIGIT_1_ON;
	}
	else if(digit == 2)
	{
		DIGIT_2_ON;
	}
}

uint8_t seg_led_itodc(const uint8_t i)
{
	uint8_t map[10] = {
		((1 << LED_A)|(1 << LED_B)|(1 << LED_C)|(1 << LED_D)|(1 << LED_E)|(1 << LED_F)),	/* 0 */
		((1 << LED_B)|(1 << LED_C)),								/* 1 */
		((1 << LED_A)|(1 << LED_B)|(1 << LED_D)|(1 << LED_E)|(1 << LED_G)),			/* 2 */
		((1 << LED_A)|(1 << LED_B)|(1 << LED_C)|(1 << LED_D)|(1 << LED_G)),			/* 3 */
		((1 << LED_B)|(1 << LED_C)|(1 << LED_F)|(1 << LED_G)),					/* 4 */
		((1 << LED_A)|(1 << LED_C)|(1 << LED_D)|(1 << LED_F)|(1 << LED_G)),			/* 5 */
		((1 << LED_A)|(1 << LED_C)|(1 << LED_D)|(1 << LED_E)|(1 << LED_F)|(1 << LED_G)),	/* 6 */
		((1 << LED_A)|(1 << LED_B)|(1 << LED_C)),						/* 7 */
		((1 << LED_A)|(1 << LED_B)|(1 << LED_C)|(1 << LED_D)|(1 << LED_E)|(1 << LED_F)|(1 << LED_G)),	/* 8 */
		((1 << LED_A)|(1 << LED_B)|(1 << LED_C)|(1 << LED_D)|(1 << LED_F)|(1 << LED_G))		/* 9 */
	};

	return map[i];
}

void seg_led_write(const uint8_t d)
{
	(d & (1 << 7)) ? LED_DP_ON : LED_DP_OFF;
	(d & (1 << 6)) ? LED_G_ON : LED_G_OFF;
	(d & (1 << 5)) ? LED_F_ON : LED_F_OFF;
	(d & (1 << 4)) ? LED_E_ON : LED_E_OFF;
	(d & (1 << 3)) ? LED_D_ON : LED_D_OFF;
	(d & (1 << 2)) ? LED_C_ON : LED_C_OFF;
	(d & (1 << 1)) ? LED_B_ON : LED_B_OFF;
	(d & (1 << 0)) ? LED_A_ON : LED_A_OFF;
}

void seg_led_update(void)
{
}

void disp_init(void)
{
	seg_led_init();
}

void disp_clear(void)
{
	seg_led_off(0);
}
