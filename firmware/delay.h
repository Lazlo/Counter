/**
 * \file
 */

/** Wraps delay function. */
void delay_long(uint8_t i);
