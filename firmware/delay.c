/**
 * \file
 */

/*
 * Includes (used from inside functions)
 */

#include <util/delay.h>

void delay_long(uint8_t wait)
{
	while(--wait)
		_delay_ms(1);
}
