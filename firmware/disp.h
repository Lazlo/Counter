/**
 * \file
 */

#ifndef D_DISP_H
#define D_DISP_H

/*
 * Macro Constants (used for configuration)
 */

/** Common Anode */
#define SEG_LED_COM_VCC		1

/** Common Cathode */
#define SEG_LED_COM_GND		0

/*
 * Includes
 */

#include <stdint.h> /* for uint8_t in function prototypes */

/*
 * Prototypes for public functions.
 */

/** Initialize 7-segment display. */
void seg_led_init(void);

/** Turn individual LED segment on. */
void seg_led_on(const uint8_t digit);

/** Turn individual LED segment off. */
void seg_led_off(const uint8_t digit);

/** Convert unsigned integer to display byte for the corresponding character. */
uint8_t seg_led_itodc(const uint8_t i);

/** Write display character to display. */
void seg_led_write(const uint8_t d);

void seg_led_update(void);

/** Initialize display. */
void disp_init(void);

/** Clear display. */
void disp_clear(void);

#endif /* D_DISP_H */
