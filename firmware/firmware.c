/**
 * \file
 */

/**
 * \mainpage
 *
 * Counter Firmware for Atmel AVR ATtiny2313 (using internal osc. at 1 MHz)
 *
 * Counts the numer of pin change interrupts and displays them
 * on a 7-segment LED display.
 */

/*
 * Includes
 */

#include "config_counter.h"
#include "pins.h"
#include "keys.h"
#include "disp.h"
#include "disp_macros.h"	/* for LED_DP_ON and LED_DP_OFF (temporary) */
#include "counter.h"

#include <avr/interrupt.h>

/*
 * Private Variables
 */

static volatile uint8_t disp_char[2];

/*
 * Function Prototypes
 */

/** Initialize system. */
void init(void);

/** Point of entry for program execution. */
int main(void);

/*
 * Functions
 */

int main(void)
{
	#include "delay.h"

	uint8_t tmp_cnt;

	uint8_t key;
	uint8_t keys_pressed;

	keys_pressed = 0;

	init();

	/* Configure hardware timer for periodic overflow interrupts. */

	TIMSK |= (1 << TOIE0); /* Set Timer Overflow Interrupt Enable bit. */
	TCCR0B |= (1 << CS00); /* Set Timer0 prescaler to 1 (will start the timer). */

	/* Enable interrupts globally. */
	sei();

	while(1)
	{
		/* Read switches. */
		keys_pressed = keys_read();

		/* Advance counter. */
		counter_tick();

		/* Create copy of current counter value. */
		tmp_cnt = counter_read();

		/* Split counter value into digits and convert each digit
		 * to a display character byte (not related to char). */
		disp_char[1] = seg_led_itodc(tmp_cnt / 10);
		disp_char[0] = seg_led_itodc(tmp_cnt % 10);

		/* Process keys read. */
		for(key = 2; key > 0; --key)
		{
			if(keys_pressed & (1 << (key-1)))
			{
				counter_reset();

				/* Activate decimal point segment when key N is pressed. */
				disp_char[key-1] |= (1 << LED_DP);

				/* Reset key pressed bit when work is done. */
				keys_pressed &= ~(1 << (key-1));
			}
		}

		delay_long(50);
	}

	return 0;
}

void init(void)
{
	/*
	 * Setup Inputs
	 */

	/* Initialize software counter and counter input pin. */
	counter_init();

	/* Setup push-button switch input pins. */
	keys_init();

	/*
	 * Setup Outputs
	 */

	/* Setup 7-segment LED display. */
	disp_init();

	/* Turn all LEDs of the 7-segment display off on startup. */
	disp_clear();
}

/** Timer0 (8-bit) Overflow Interrupt service routine.
 *
 * Is responsible for drawing the characters to the display.
 *
 * TODO due to the loop nature of the multiplexing code
 * we have written, the brightness of the digits is like
 * 10% for the 2nd digit and 90% for the 1st.
 * This is caused by having a delay in the main loop.
 * To fix this, the ISR should not loop over digits
 * but draw only one digit at a time. This should even
 * out the brightness.
 */

ISR(TIMER0_OVF_vect)
{
	uint8_t digit;

	for(digit = 2; digit > 0; --digit)
	{
		/* Tunr all digits off. */
		disp_clear();

		/* Write character byte to digit on display. */
		seg_led_write(disp_char[digit-1]);

		/* Activate digit on display. */
		seg_led_on(digit);
	}
}
