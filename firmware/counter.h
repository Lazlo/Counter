/**
 * \file
 */

#ifndef D_COUNTER_H
#define D_COUNTER_H

/**
 * \brief Initialize software counter.
 */
void counter_init(void);

/**
 * \brief Reset counter value.
 */
void counter_reset(void);

/**
 * \brief Advance counter by one tick.
 */
void counter_tick(void);

/**
 * \brief Get current counter value.
 */
uint8_t counter_read(void);

#endif /* D_COUNTER_H */
