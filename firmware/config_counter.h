/**
 * \file
 */

#ifndef D_CONFIG_COUNTER_H
#define D_CONFIG_COUNTER_H

/**
 * \brief Largest number that can be shown on the display.
 */
#define COUNTER_MAX	99

#endif /* D_CONFIG_COUNTER_H */
