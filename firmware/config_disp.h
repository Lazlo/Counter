/**
 * \file
 */

#ifndef D_CONFIG_DISP_H
#define D_CONFIG_DISP_H

/**
 * \brief Specify the mode to select a digit on the display.
 *
 * Is either SEG_LED_COM_GND for common cathode and SEG_LED_COM_VCC for
 * common anode types.
 */
#define SEG_LED_MODE		SEG_LED_COM_GND

#endif /* D_CONFIG_DISP_H */
