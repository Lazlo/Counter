/**
 * \file
 */

#ifndef D_CONFIG_PINS_H
#define D_CONFIG_PINS_H

#include "platform.h"

/*
 * Configuration
 */

/* 7 Segment LED Pins */

#define LED_DP			7		/**< pin offset for the "decimal point"-segment LED. */
#define LED_DP_DDR		DDRB		/**< pin direction register for "decimal point"-segment LED. */
#define LED_DP_PORT		PORTB		/**< pin output register for "decimal point"-segment LED. */

#define LED_G			6		/**< pin offset for "G"-segment LED. */
#define LED_G_DDR		DDRB		/**< pin direction register for "G"-segment LED. */
#define LED_G_PORT		PORTB		/**< pin output register for "G"-segment LED. */

#define LED_F			5		/**< pin offset for "F"-segment LED. */
#define LED_F_DDR		DDRB		/**< pin direction register for "F"-segment LED. */
#define LED_F_PORT		PORTB		/**< pin output register for "F"-segment LED. */

#define LED_E			4		/**< pin offset for "E"-segment LED. */
#define LED_E_DDR		DDRB		/**< pin direction register for "E"-segment LED. */
#define LED_E_PORT		PORTB		/**< pin output register for "E"-segment LED. */

#define LED_D			3		/**< pin offset for "D"-segment LED. */
#define LED_D_DDR		DDRB		/**< pin direction register for "D"-segment LED. */
#define LED_D_PORT		PORTB		/**< pin output register for "D"-segment LED. */

#define LED_C			2		/**< pin offset for "C"-segment LED. */
#define LED_C_DDR		DDRB		/**< pin direction register for "C"-segment LED. */
#define LED_C_PORT		PORTB		/**< pin output register for "C"-segment LED. */

#define LED_B			1		/**< pin offset for "B"-segment LED. */
#define LED_B_DDR		DDRB		/**< pin direction register for "B"-segment LED. */
#define LED_B_PORT		PORTB		/**< pin output register for "B"-segment LED. */

#define LED_A			0		/**< pin offset for "A"-segment LED. */
#define LED_A_DDR		DDRB		/**< pin direction register for "A"-segment LED. */
#define LED_A_PORT		PORTB		/**< pin output register for "A"-segment LED. */

/* Digit Select Pins */

#define DIGIT_2			6		/**< pin offset for digit 2 input pin. */
#define DIGIT_2_DDR		DDRD		/**< pin direction register for digit 2 input pin. */
#define DIGIT_2_PORT		PORTD		/**< pin output register for digit 2 input pin. */

#define DIGIT_1			5		/**< pin offset for digit 1 input pin. */
#define DIGIT_1_DDR		DDRD		/**< pin direction register for digit 1 input pin. */
#define DIGIT_1_PORT		PORTD		/**< pin output register for digit 1 input pin. */

/* Push-Button Switches */

#define SW2			4		/**< pin offset for switch 2 input pin. */
#define SW2_DDR			DDRD		/**< pin direction register for switch 2 input pin. */
#define SW2_PORT		PORTD		/**< pin output register for switch 2 input pin. */
#define SW2_PIN			PIND		/**< pin input register for swtich 2 input pin. */

#define SW1			3		/**< pin offset for switch 1 input pin. */
#define SW1_DDR			DDRD		/**< pin direction register for switch 1 input pin. */
#define SW1_PORT		PORTD		/**< pin output register for switch 1 input pin. */
#define SW1_PIN			PIND		/**< pin input register for switch 1 input pin. */

/* Counter Input Pin */

#define CNT_IN			2		/**< pin offset for coutner input pin. */
#define CNT_IN_DDR		DDRD		/**< pin direction register for counter input pin. */
#define CNT_IN_PORT		PORTD		/**< pin output register for counter input pin. */
#define CNT_IN_PIN		PIND		/**< pin input register for counter input pin. */

#endif /* D_CONFIG_PINS_H */
