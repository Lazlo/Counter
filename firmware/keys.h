/**
 * \file
 * \brief Keys module interface
 *
 * Handles setting up and reading the push button switches.
 */

#ifndef D_KEYS_H
#define D_KEYS_H

/**
 * \brief Initialize keys module.
 *
 * Will setup pins and module state.
 */
void keys_init(void);

/**
 * \brief Read all key states.
 *
 * Will return a byte where the LSB is used for the first key and the MSB for the last.
 */
uint8_t keys_read(void);

#endif /* D_KEYS_H */
