#!/bin/bash

set -e
set -u

pkgs=""
pkgs="$pkgs make"
pkgs="$pkgs avr-libc"
pkgs="$pkgs gcc-avr"
pkgs="$pkgs binutils-avr"
pkgs="$pkgs doxygen"

apt-get -q install -y $pkgs
